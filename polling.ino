#define zece_secunde 6000 // 10 secunde
#define trei_secunde 3000 // 3 secunde
#define exceptie 2000 // 2 secunde
#define minut 15000 // un minut

void setup() {
  /*
  senzor presiune gaz: A0
  senzor temperatura: A1
  senzor presiune gaz evacuare: A2
  senzor ionic: A3

  ventilator evacuara gaz: A4
  electrovalva gaz: A5
  aprinzator scanteie: A6

  excpetie1: A7
  exceptie2: A8
  */
  //inputs:
  pinMode(A0,INPUT_PULLUP);
  pinMode(A1,INPUT_PULLUP);
  pinMode(A2,INPUT_PULLUP);
  pinMode(A3,INPUT_PULLUP);
  //outputs:
  pinMode(A4,OUTPUT);
  pinMode(A5,OUTPUT);
  pinMode(A6,OUTPUT);
  //exceptions:
  pinMode(A7,OUTPUT);
  pinMode(A8,OUTPUT);
  //test leds:
  digitalWrite(A4,HIGH);
  digitalWrite(A5,HIGH);
  digitalWrite(A6,HIGH);
  digitalWrite(A7,HIGH);
  digitalWrite(A8,HIGH);
  delay(1000);
  digitalWrite(A4,LOW);
  digitalWrite(A5,LOW);
  digitalWrite(A6,LOW);
  digitalWrite(A7,LOW);
  digitalWrite(A8,LOW);
  //automaton:
  start:
  if(digitalRead(A0) == 0){
    if(digitalRead(A1)){
      digitalWrite(A4,HIGH);
      delay(minut);
      if(digitalRead(A2)){
        digitalWrite(A6,HIGH);
        digitalWrite(A5,HIGH);
        before_wait_3sec:
        delay(trei_secunde);
        if(digitalRead(A3) == 0){
          digitalWrite(A6,LOW);//stop scanteie
          if(digitalRead(A1)){
            goto before_wait_3sec;
          } else {
            digitalWrite(A5,LOW);
            digitalWrite(A4,LOW);
            goto before_wait_1min;
          }
        } else{
          //exceptie2
          digitalWrite(A4,LOW);
          digitalWrite(A5,LOW);
          digitalWrite(A6,LOW);
          digitalWrite(A8,HIGH);
          delay(exceptie);
          digitalWrite(A8,LOW);
          goto start;
        }
      } else{
        //exceptie1
        digitalWrite(A4,LOW);
        digitalWrite(A7,HIGH);
        delay(exceptie);
        digitalWrite(A7,LOW);
        goto start;
      }
    } else {
      before_wait_1min:
      delay(minut);
      goto start;
    }
  } else {
    delay(zece_secunde);
    goto start;
  }
}

void loop() {
  
}
